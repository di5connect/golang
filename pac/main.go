package pac

import (
	"strings"
	"gitlab.com/stud777/stuff/person"
	"github.com/essentialkaos/translit/v2"
)

type Person struct {
	name string 
	surname string 
	midname string
}

const (
	nullWalue = "NULL"
)

//func CreateUserDataString(){
//
//}

func GetUserDataFromString(userDataString string)(person.UserData){
// или или 
//func CreateUserData()(person.UserData, error){	
	//mtd := person.Metodist{}
	//str, err := mtd.GetUserDataString()
	//if err != nil {
	// 	panic(err)
	//}

	usdDate := person.UserData{}
	strAsSlice := strings.Split(userDataString, " ")
	
	usdDate.Surname = strAsSlice[0]
	usdDate.Name = strAsSlice[1]
	usdDate.Midname = strAsSlice[2]

	if strAsSlice[3]!=nullWalue{
		usdDate.SetEmail(strAsSlice[3])
	} 
	if strAsSlice[4]!=nullWalue{
		usdDate.SetPasswd(strAsSlice[4])
	} 
	
	if len(strAsSlice) > 5 && len(strAsSlice[5]) > 0 {
		usdDate.IsProfessor = true
	}

	return usdDate
}

func GetMoodleUsername(ud person.UserData) string{
	
	moodleName := ""

	if len(strings.TrimSpace(ud.Surname)) > 0 {
		moodleName += translit.EncodeToICAO(strings.ToLower(ud.Surname))
	}
	if len(strings.TrimSpace(ud.Name)) > 0 {
		var a string = translit.EncodeToICAO(strings.ToLower(ud.Name))
		moodleName += string(a[0])	
	}
	if len(strings.TrimSpace(ud.Midname)) > 0 {
		var a string = translit.EncodeToICAO(strings.ToLower(ud.Midname))
		moodleName += string(a[0])		
	}

	return moodleName
}

func main (){
	person := GetUserDataFromString("a b c abc@mail.ru asda345")

	print (person.Surname, " ", person.Name, " ", person.Midname, " ", person.Email(), " ", person.Passwd(), " ", person.IsProfessor, "\n")

	userName := GetMoodleUsername(person)
	print (userName)
}